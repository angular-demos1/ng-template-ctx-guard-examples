import {ChangeDetectionStrategy, Component, contentChild, input, InputSignal, Signal, TemplateRef} from '@angular/core';
import {PdListDirective} from "./pd-list.directive";
import {AppTemplateOutlet} from "../pd-template-outlet.directive";

@Component({
  selector: 'app-list',
  standalone: true,
  imports: [
    AppTemplateOutlet

  ],
  templateUrl: './list.component.html',
  styleUrl: './list.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ListComponent<T extends object> {
  data: InputSignal<T[]> = input.required<T[]>();
  dataRef: Signal<TemplateRef<any>> = contentChild.required(PdListDirective, {read: TemplateRef})
}
