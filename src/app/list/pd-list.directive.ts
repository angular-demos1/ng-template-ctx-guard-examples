import {Directive, input, InputSignal} from '@angular/core';

interface ListContext<T> {
  $implicit: T,
  pdList: T,
  i: number
}

@Directive({
  selector: 'ng-template[pdList]',
  standalone: true
})
export class PdListDirective<T extends object> {
  pdList: InputSignal<T[]> = input.required<T[]>()

  static ngTemplateContextGuard<CTX extends object>(
    directive: PdListDirective<CTX>,
    context: unknown
  ): context is ListContext<CTX> {
    return true
  }
}
