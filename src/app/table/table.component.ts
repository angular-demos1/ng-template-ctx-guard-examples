import {Component, contentChild, ContentChild, input, Input, InputSignal, Signal, TemplateRef} from '@angular/core';
import {NgForOf, NgTemplateOutlet} from "@angular/common";
import {TableHeadDirective} from "./table-head.directive";
import {TableRowDirective} from "./table-row.directive";

@Component({
  selector: 'app-table',
  standalone: true,
  imports: [
    NgTemplateOutlet,
    NgForOf,
    TableHeadDirective,
    TableRowDirective
  ],
  templateUrl: './table.component.html',
  styleUrl: './table.component.scss'
})
export class TableComponent<T extends object> {
  data: InputSignal<T[]> = input.required<T[]>();
  tableHead: Signal<TemplateRef<TableHeadDirective>> = contentChild.required(TableHeadDirective, {read: TemplateRef});
  tableRow: Signal<TemplateRef<any>> = contentChild.required(TableRowDirective, {read: TemplateRef});
}
