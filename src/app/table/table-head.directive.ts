import { Directive } from '@angular/core';

@Directive({
  selector: 'ng-template[appTableHead]',
  standalone: true
})
export class TableHeadDirective {
  constructor() { }
}
