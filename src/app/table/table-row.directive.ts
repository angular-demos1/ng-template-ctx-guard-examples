import {Directive, input, InputSignal} from '@angular/core';

interface Row<T extends object> {
  $implicit: T
}

@Directive({
  selector: 'ng-template[appTableRow]',
  standalone: true
})
export class TableRowDirective<T extends object> {
  appTableRow: InputSignal<T[]> = input.required<T[]>();

  static ngTemplateContextGuard<CTX extends object>(
    directive: TableRowDirective<CTX>,
    context: unknown
  ): context is Row<CTX> {
    return true;
  }
}
