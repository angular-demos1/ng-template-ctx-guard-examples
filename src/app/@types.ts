import {interval, Subscription, tap} from "rxjs";
import {Signal, signal, WritableSignal} from "@angular/core";

export interface Person {
  firstName: string;
  lastName: string;
  age: number;
}

export interface Drivable {
  drive: VoidFunction
  stop: VoidFunction
  model: string;
}

export abstract class Car implements Drivable {
  #subscription: Subscription = new Subscription();
  #km: WritableSignal<number>;
  model: string;
  isDriving: boolean

  protected constructor(model: string) {
    this.#km = signal(0);
    this.model = model;
    this.isDriving = false;
  }

  get km(): Signal<number> {
    return this.#km.asReadonly();
  }

  drive(): void {
    if (this.isDriving) {
      return;
    }

    const obs$: Subscription = interval(1).pipe(
      tap({next: (_) => this.#km.update(crr => crr + 1)})
    ).subscribe();

    this.#subscription.add(obs$)
    this.isDriving = true;
  }

  stop(): void {
    this.#subscription.unsubscribe();
    this.#subscription = new Subscription();
    this.isDriving = false;
  };
}

export abstract class Motorcycle implements Drivable {
  model: string;

  constructor(model: string) {
    this.model = model;
  }

  drive() {
    console.log("drive motorcycle")
  }

  stop() {
    console.log("stop motorcycle")
  };

  driveOnOneWheel() {
    console.log("drive on one wheel")
  }
}

export class Toyota extends Car {
  constructor(model: string) {
    super(model);
  }
}

export class Nissan extends Car {
  constructor(model: string) {
    super(model);
  }
}

export class HarleyDavidson extends Motorcycle {
  constructor(model: string) {
    super(model);
  }
}


type MyVehicle = Toyota | Nissan | HarleyDavidson;

function isToyota(car: Drivable): car is Toyota {
  return car instanceof Toyota;
}

function isNissan(car: Drivable): car is Nissan {
  return car instanceof Nissan;
}

function isHarleyDavidson(motorcycle: Drivable): motorcycle is HarleyDavidson {
  return motorcycle instanceof HarleyDavidson;
}

export function checkMyVehicle(vehicle: MyVehicle) {
  if (isToyota(vehicle)) {
    console.log("is Toyota");
  }

  if (isNissan(vehicle)) {
    console.log("is Nissan");
  }

  if (isHarleyDavidson(vehicle)) {
    console.log("is HarleyDavidson");
    vehicle.driveOnOneWheel();
  }
}

