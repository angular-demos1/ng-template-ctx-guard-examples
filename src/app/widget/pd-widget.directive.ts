import {Directive, input, InputSignal} from '@angular/core';
import {Drivable, Person} from "../@types";

interface PdWidgetContext {
  owner: Person;
  vehicle: Drivable;
}

@Directive({
  selector: 'ng-template[pdWidget]',
  standalone: true
})
export class PdWidgetDirective {
  owner: InputSignal<Person> = input.required<Person>();
  vehicle: InputSignal<Drivable> = input.required<Drivable>();

  static ngTemplateContextGuard(
    dir: PdWidgetDirective,
    ctx: unknown
  ): ctx is PdWidgetContext {
    return true;
  }
}

