import {Component} from '@angular/core';
import {Drivable, Nissan, Person} from "../@types";
import {PdWidgetDirective} from "./pd-widget.directive";
import {NgTemplateOutlet} from "@angular/common";

@Component({
  selector: 'pd-widget',
  standalone: true,
  imports: [
    PdWidgetDirective,
    NgTemplateOutlet
  ],
  templateUrl: './widget.component.html',
  styleUrl: './widget.component.scss'
})
export class WidgetComponent {
  owner: Person = {firstName: 'John', lastName: 'Doe', age: 30};
  vehicle: Drivable = new Nissan('GTR');
}
