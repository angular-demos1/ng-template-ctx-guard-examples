import {Component} from '@angular/core';
import {RouterOutlet} from '@angular/router';
import {TableComponent} from "./table/table.component";
import {TableHeadDirective} from "./table/table-head.directive";
import {TableRowDirective} from "./table/table-row.directive";
import {ListComponent} from "./list/list.component";
import {PdListDirective} from "./list/pd-list.directive";
import {Car, checkMyVehicle, HarleyDavidson, Nissan, Person, Toyota} from "./@types";
import {WidgetComponent} from "./widget/widget.component";

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet, TableComponent, TableHeadDirective, TableRowDirective, ListComponent, PdListDirective, WidgetComponent],
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss'
})
export class AppComponent {
  persons: Person[] = [
    {firstName: 'John', lastName: 'Doe', age: 30},
    {firstName: 'Jane', lastName: 'Doe', age: 25},
    {firstName: 'Jim', lastName: 'Smith', age: 40}
  ];

  cars: Array<Car> = [
    new Toyota('Supra'),
    new Nissan('GTR'),
  ];


  constructor() {
    const harley: HarleyDavidson = new HarleyDavidson('Harley Davidson');
    checkMyVehicle(this.cars[1]);
    checkMyVehicle(harley);
  }
}
